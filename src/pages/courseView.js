import{useState, useEffect, useContext} from 'react'
import{Container,Card, Button, Row, Col } from 'react-bootstrap'
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from '../UserContext';
import Swal from 'sweetalert2'




export default function CourseView(){
	

	const { user } = useContext(UserContext);

	//Allow to gain access to methods that will allow us to redirect a user a different page after enrolling our a course
	const navigate = useNavigate() //useHistory

	//The "useParams" 
	const {courseId} = useParams()

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] =  useState(0)


	const enroll = (courseId) =>{
		fetch(`http://localhost:4000/users/enroll`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},

			body: JSON.stringify({
				courseId: courseId
			})
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)


			if(data === true){
				Swal.fire({
					title: "Successfully Enrolled",
					icon: "success",
					text: "you have successfully enrolled for this course"
				})

				navigate("/courses")

			}else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "please try again"
				})
			}
		})
	}

	useEffect(() => {
		console.log(courseId)

		fetch(`http://localhost:4000/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})


	}, [courseId])

	return (

		<Container className="mt-3">
		<Row>
			<Col lg={{span:6, offset:3}}>
				     <Card>
            <Card.Body className="text-center">
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                <Card.Subtitle>Class Schedule:</Card.Subtitle>
                <Card.Text>8AM - 5PM</Card.Text>
               {
               	user.id !== null ?
               	 <Button variant="primary" onClick={() => enroll(courseId)}>Enroll</Button>
               	 :
               	  <Link className="btn btn-danger" to="/login">Log in to Enroll </Link>
               }
            </Card.Body>
					</Card>
			</Col>
		</Row>

		</Container>


		)
}