
import SampleCourse from '../Components/SampleCourse'
// import coursesData from '../data/coursesData'
import {Fragment, useEffect, useState,} from 'react'

export default function Courses() {

	// console.log(coursesData)
	// console.log(coursesData[0])

	const [courses, setCourses] = useState([])

	useEffect(() => {

		fetch('http://localhost:4000/courses/')
		.then(res => res.json())
		.then(data => {
			console.log(data)




			setCourses(data.map(course => {

				return (
						<SampleCourse key={course._id} courseProp={course} />
					)
			}))
		})

	}, [])


	// const courses = coursesData.map(course => {
	// 	return (
	// 	<SampleCourse key={course.id} courseProp={course}/>
	// 	)
	// })



	return (

		<Fragment>
				<h1>courses</h1>
				{courses}
		</Fragment>

		)
}