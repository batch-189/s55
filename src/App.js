import {Container} from 'react-bootstrap'
import {BrowserRouter as Router} from 'react-router-dom'
import {Routes, Route} from 'react-router-dom'
import {Fragment} from 'react'
import {useState, useEffect} from 'react'
import AppNavbar from './Components/AppNavbar';
import Home from './pages/home'
import Logout from './pages/logout'
import Courses from './pages/courses'
import Register from './pages/register'
import CourseView from './pages/courseView'
import Login from './pages/login'
import NotFound from './pages/notfound'
import './App.css';
import { UserProvider } from './UserContext'







function App() {
  
  //State hook for the user state that's defined here for a global scope 
  const [user, setUser] = useState({
  //   email: localStorage.getItem('email')
  id: null,
  isAdmin: null
   })

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(()=> {
    fetch('http://localhost:4000/users/details', {

      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {

      if(typeof data._id !== "undefined"){


        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })



  }, []) 


  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
    <AppNavbar/>
    <Container>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/courses" element={<Courses/>}/>
            <Route path="/courses/:courseId" element={<CourseView/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="*" element={<NotFound/>}/>
          </Routes>
          </Container>
    </Router>
    </UserProvider>



  );
}

export default App;
